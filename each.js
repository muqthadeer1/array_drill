//create a function each and has two variables elements and callback function
function each(elements, cb) {
  for (let i = 0; i < elements.length; i++) {
    cb(elements[i], i);
  }
}

//create a module to export the function name each.
module.exports = each;
