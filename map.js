//create a function name as "map" with two arguements elements and callback function
function map(elements, cb) {
  // create a variable with empty array
  let newArr = [];

  for (let i = 0; i < elements.length; i++) {
    newArr.push(cb(elements[i]));
  }
  //it return newArr
  return newArr;
}

//create a module to export the function named as map
module.exports = map;
