//create a variable to store the function which is imports from other directory
let fnReduce = require("../reduce.js");

//create a callback function with two arguements
function callBack(accumulator, currentValue) {
  return accumulator + currentValue;
}

// create a variable to execute the function which is imported
let reduce = fnReduce([1, 2, 3, 4, 5], callBack, 0);

//it prints output.
console.log(reduce);
  