//create the function named fnfind which import the function from the other directory
let fnFind = require("../find");

//create a callBack function
function cb(value) {
  // it returns the value which is divisible by 2
  return value % 2 === 0;
}
//create variable to execute the imported function and store the output
let findEvenNumbers = fnFind([2, 4, 6, 8], cb);

//it prints the output 
console.log(findEvenNumbers);
