//create variable fnEach to import a function from other directory
let fnEach = require("../each.js");

//create a function callBack with two arguements
function callback(value, index) {
  // it prints the value and index value
  console.log(value + " " + index);
}

// create a variable callFn to execute the function which is imported
let callFn = fnEach([1, 2, 3, 4, 5], callback); 
