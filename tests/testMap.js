//create a variable to import the function from other directory
let fnMap = require("../reduce.js");

//create a callBack function
function callback(value) {
  return value * value;
}

//create a variable to execute function which import from other directory
let callFn = fnMap([1, 2, 3, 4, 5], callback);

//it prints output
console.log(callFn); 
