// create variable fnFilter to import the function from other directory
let fnFilter = require("../filter.js");

// create a call function name as cb with one arguement
function cb(value) {
  //it return a value which is divisible by 2
  return value % 2 == 0;
}

// create a callfunction to execute the function which is imported
let callFunction = fnFilter([2, 4, 6, 8], cb);

//it prints the output
console.log(callFunction); 
