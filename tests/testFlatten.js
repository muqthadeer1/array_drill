// create variable to import the function from other directory
let fnflatten = require("../flatten.js");

//create the variable to execute the function
let flattenArr = fnflatten([1, [2], [[3]], [[[4]]]]);

//it prints output
console.log(flattenArr);
 