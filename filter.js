// create a function filter with two arguements elements and a callBack
function filter(elements, cb) {
  //create a empty array to store the result
  let newArr = [];

  // iterate the loop upto end of the array
  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index], index)) {
      // push the value one by one in newArr
      newArr.push(elements[index]);
    }
  }
  // it returns the newArr
  return newArr;
}
// create a module to export the function named as filter.
module.exports = filter;
