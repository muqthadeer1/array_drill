// create function reduce
function reduce(elements, callBack, startingPoint) {
  //create a variable
  let accumulator = 0;

  //if starting point is not equal to undefined
  if (startingPoint !== undefined) {
    accumulator = startingPoint;
  } else {
    accumulator = elements[0];
  }
  for (let index = 0; index < elements.length; index++) {
    accumulator = callBack(accumulator, elements[index]);
  }
  return accumulator;
}

// create a module to export the function to other file.
module.exports = reduce;
