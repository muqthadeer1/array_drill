//create a function flatten with one arguement
function flatten(arr) {
  //create variable flattenArr with empty array
  let flattenArr = [];
  for (let index = 0; index < arr.length; index++) {
    if (Array.isArray(arr[index])) {
      flattenArr = flattenArr.concat(flatten(arr[index]));
    } else {
      flattenArr.push(arr[index]);
    }
  }

  return flattenArr;
}
//create a module to exports the function to other directory
module.exports = flatten;
