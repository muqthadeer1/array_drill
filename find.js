// create a function find with two arguements elements and callBack function
function find(elements, cb) {
  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index])) {
      return elements[index];
    }
  }
  return undefined;
}

//create the module to exports the function find to other file.
module.exports = find;
